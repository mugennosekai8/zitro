//load.js
var loadState={
	preload: function(){

 		game.load.image('background', 'images/background.jpg');
      	game.load.image('logo', 'images/logo.png');

		game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		game.scale.PageAlignHorizonally = true;
		game.scale.PageAlignVertically = true;

		
		game.load.image('simbolos', 'images/simbolos.png');
		game.load.image('fondo', 'images/fondo.png');
		game.load.image('spin', 'images/spin.png');


 	},
 	create: function(){
 		var back=game.add.tileSprite(0, 0, game.width, game.height, 'background');
        var logo= game.add.image((game.width/2)-128, (game.height/2)-128, 'logo');
        var loadingLabel = game.add.text((game.width/2)-64, (game.height/2)+140, 'loading...',{font: '30px Courier'});

        logo.scale.set(0.5);
        
 	 	game.time.events.add(Phaser.Timer.SECOND * 4, Start, this);

	}
 };

function Start() {
	
	game.state.start('play');

}