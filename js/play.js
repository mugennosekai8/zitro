var playState = {
 col1:null,
 col2:null,
 col3:null,
 active1:false,
 active2:false,
 active3:false,
 position:[34,102,169,237,305,372,440,508,575],
  preload: function(){
		game.load.json('data', 'https://worldtimeapi.org/api/ip');
  },
  create: function() {
  	var self = this;

    var fondo=game.add.tileSprite(0, 0, game.width, game.height, 'fondo');
    var data = game.cache.getJSON('data');
    var date = game.add.text((game.width/2)-64, 50, data.datetime  ,{font: '10px ', fill: '#ffffff'});

	self.col1= new Col(62);
	self.col2 = new Col(166);
	self.col3 = new Col(270);

    var button = game.add.button((game.width/2) - 63, game.height-200, 'spin', Spin, this, 2, 1, 0);

  
    
  },
  render: function(){
   
  },
  update: function() {
  	var self = this;

  	if(self.active1){
  		self.col1.tilePosition.y += 12;
  	}
	if(self.active2){
		self.col2.tilePosition.y += 12;
	}
	if(self.active3){
		self.col3.tilePosition.y += 12;
	}
  		
  
  }
  
};


function Spin(){
  	this.active1=true;
  	var num= this.game.rnd.pick(this.position);
  	this.col1.tilePosition.y = num;
  	this.game.time.events.add(Phaser.Timer.SECOND * 0.5, Spin2, this);
  	this.game.time.events.add(Phaser.Timer.SECOND * 3, Stop, this);
 
}
function Spin2(){
	this.active2=true;
	var num= this.game.rnd.pick(this.position);
  	this.col2.tilePosition.y = num;
	this.game.time.events.add(Phaser.Timer.SECOND * 0.5, Spin3, this);
  	this.game.time.events.add(Phaser.Timer.SECOND * 3, Stop2, this);
}
function Spin3(){
	this.active3=true;
	var num= this.game.rnd.pick(this.position);
  	this.col3.tilePosition.y = num;
  	this.game.time.events.add(Phaser.Timer.SECOND * 3, Stop3, this);
}

function Stop(){
	this.active1=false;
	this.col1.tilePosition.y = Phaser.ArrayUtils.findClosest(this.col1.tilePosition.y,this.position);
	
}
function Stop2(){
	this.active2=false;
	this.col2.tilePosition.y = Phaser.ArrayUtils.findClosest(this.col2.tilePosition.y,this.position);;
}
function Stop3(){
	this.active3=false;
	this.col3.tilePosition.y = Phaser.ArrayUtils.findClosest(this.col3.tilePosition.y,this.position);;
}

function Col(x) {
	
	var col =game.add.tileSprite(0, 0, 60, 131, 'simbolos');

	col.x=x;
	col.y=225;
	col.scale.set(1.40);
	col.tilePosition.y = 34;

	return col;
}
 
